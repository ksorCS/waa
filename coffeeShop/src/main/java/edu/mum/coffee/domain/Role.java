package edu.mum.coffee.domain;

public enum Role {
	ADMIN, CUSTOMER;
}
