package edu.mum.coffee.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Orderline")
@Getter
@Setter
@NoArgsConstructor
public class Orderline {

	@Id
	@GeneratedValue
	private int id;
	private int quantity;
	@OneToOne
	private Product product;
	@ManyToOne
	private Order order;

	public double getSubtotal() {
		return quantity * product.getPrice();
	}

	public double getPrice() {
		return product.getPrice();
	}

}
