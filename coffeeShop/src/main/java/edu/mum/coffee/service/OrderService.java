package edu.mum.coffee.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.mum.coffee.domain.Order;
import edu.mum.coffee.domain.Orderline;
import edu.mum.coffee.domain.Person;
import edu.mum.coffee.domain.Product;
import edu.mum.coffee.repository.OrderRepository;

@Service
@Transactional
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	
	public Order save(Order order){
		return orderRepository.save(order);
	}
	
	public void delete(Order order){
		orderRepository.delete(order);
	}
	
	public List<Order> findByProduct(Product product) {
		return orderRepository.findDistinctOrderByOrderLines_Product(product);
	}
	
	public List<Order> findByPerson(Person person) {
		return orderRepository.findOrderByPerson(person);
	}

	public List<Order> findByDate(Date minDate, Date maxDate) {
		return orderRepository.findOrderByOrderDateBetween(minDate, maxDate);
	}

	public Order findById(int id){
		return orderRepository.getOne(id);
	}

	public List<Order> findAll(){
		return orderRepository.findAll();
	}
	

	public void updateOrder(Order order, Product product) {
		if (product != null) {
			boolean existing = false;
			for (Orderline ol : order.getOrderLines()) {
				if (ol.getProduct().getId() == product.getId()) {
					ol.setQuantity(ol.getQuantity() + 1);
					existing = true;
					break;
				}
			}
			if (!existing) {
				Orderline orderline = new Orderline();
				orderline.setProduct(product);
				orderline.setQuantity(1);
				order.addOrderLine(orderline);
			}
		}
	}
	
	public void updateOrderLine(Order order, int productId, int quantity) {
		for (Orderline ol : order.getOrderLines()) {
			if (ol.getProduct().getId() == productId) {
				ol.setQuantity(quantity);
				break;
			}
		}
	}
	
	public void removeOrderLine(Order order, int productId) {
		for (Orderline ol : order.getOrderLines()) {
			if (ol.getProduct().getId() == productId) {
				order.removeOrderLine(ol);
				break;
			}
		}
	}

}
