package edu.mum.coffee.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import edu.mum.coffee.domain.Role;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	DataSource dataSource;
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/home", "/index","/user/register").permitAll()
                .antMatchers("/css/**", "/img/**", "/js/**").permitAll()
                .antMatchers("/orders/viewOrders", "/orders/addOrderLine", "/orders/update").permitAll()
                .antMatchers("/orders/placeOrder", "/orders/confirmOrder", "/people/**").hasAuthority(Role.CUSTOMER.name())
                .antMatchers("/**/api/**").permitAll()
                .antMatchers("/admin/**").hasAuthority(Role.ADMIN.name())
                .anyRequest().authenticated()
                .and()
            .csrf()
            	.ignoringAntMatchers("/**/api/**")
            	.and()
            .formLogin()
            	.loginPage("/user/login")
            	.loginProcessingUrl("/login")
            	.permitAll()
            	.and()
            .logout()
            	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            	.logoutSuccessUrl("/")
                .permitAll();
    }

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
			final String DEF_USERS_BY_USERNAME_QUERY =
				"select p.email,u.password,p.enable " +
				"from user u, person p " +
				"where p.email = ? "+
				"and u.person_id = p.id";
			final String DEF_AUTHORITIES_BY_USERNAME_QUERY =
				"select p.email,u.role " +
				"from user u, person p " +
				"where p.email = ? "+
				"and u.person_id = p.id";
		auth.jdbcAuthentication()
			.dataSource(dataSource)
			.usersByUsernameQuery("")
			.usersByUsernameQuery(DEF_USERS_BY_USERNAME_QUERY)
			.authoritiesByUsernameQuery(DEF_AUTHORITIES_BY_USERNAME_QUERY);
	}
}