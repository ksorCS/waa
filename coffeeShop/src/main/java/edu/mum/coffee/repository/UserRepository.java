package edu.mum.coffee.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mum.coffee.domain.User;

public interface UserRepository extends JpaRepository<User, Serializable>{
	public User findByPersonEmail(String email);
}
