package edu.mum.coffee.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.mum.coffee.domain.Person;
import edu.mum.coffee.service.PersonService;

@Controller
@RequestMapping("/people")
@SessionAttributes(names= "personId")
public class PersonController {
	@Autowired
	private PersonService personService;
	
	@GetMapping("/viewUpdate")
	public String viewUpdatePerson(Model model, Principal principal) {
		String userName = principal.getName();
		Person person = personService.findByEmail(userName).stream().findFirst().orElse(null);
		model.addAttribute("personId",  person.getId());
		model.addAttribute("person", person);
		return "customer/userInfoUpdate";
	}
	
	@PostMapping("/update")
	public String updatePerson(Model model, Person person, HttpSession session) {
		Long Id = (Long) session.getAttribute("personId");
		person.setId(Id);
		personService.savePerson(person);
		return "redirect:/";
	}
}
