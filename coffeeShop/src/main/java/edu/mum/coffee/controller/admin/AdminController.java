package edu.mum.coffee.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import edu.mum.coffee.service.ProductService;

@Controller(value ="adminController")
public class AdminController {
	@Autowired
	private ProductService productService;
	
	@GetMapping({"/admin"})
	public String homePage(Model model) {
		model.addAttribute("products", productService.getAllProduct());
		return "redirect:/admin/products";
	}
}
