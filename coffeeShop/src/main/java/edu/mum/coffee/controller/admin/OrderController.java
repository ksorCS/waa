package edu.mum.coffee.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.mum.coffee.domain.Order;
import edu.mum.coffee.service.OrderService;

@Controller(value ="adminOrderController")
@RequestMapping("/admin/orders")
@SessionAttributes(names= "customerOrder")
public class OrderController {
	@Autowired
	private OrderService orderService;
	
	@GetMapping("")
	public String viewListPeoplePage(Model model) {
		model.addAttribute("orders", orderService.findAll());
		return "admin/order/list";
	}

	@GetMapping(value = "/remove")
	public String removeOrder(@RequestParam("id") int id) {
		Order order = orderService.findById(id);
		orderService.delete(order);
		return "redirect:/admin/orders";
	}

	@GetMapping(path="/api/all", produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Order> getAllOrders(){
		return orderService.findAll();
	}
	
	@GetMapping(path="/api/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Order getById(@PathVariable int id){
		return orderService.findById(id);
	}
	
	@PostMapping(path="/api/add", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Order addOrder(@RequestBody Order order) {
		return orderService.save(order);
	}
	
	@PostMapping(path="/api/update", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Order updateOrder(@RequestBody Order order) {
		return orderService.save(order);
	}
	
	@DeleteMapping(path ="/api/delete/{id}")
	public void deleteOrder(@PathVariable int id) {
		orderService.delete(orderService.findById(id));
	}
		
	
}
