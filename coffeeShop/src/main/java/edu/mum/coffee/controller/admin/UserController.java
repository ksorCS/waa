package edu.mum.coffee.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.mum.coffee.domain.Role;
import edu.mum.coffee.domain.User;
import edu.mum.coffee.service.UserService;

@Controller(value = "adminUserController")
@RequestMapping("/admin/user")
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("/new")
	public String register(Model model) {
		model.addAttribute("user", new User());
		return "admin/person/new";
	}

	@PostMapping("/add")
	public String saveUser(Model model, @Valid User user, Errors errors) {
		if (errors.hasErrors()) {
			return "admin/person/new";
		}
		user.setRole(Role.CUSTOMER);
		userService.saveUser(user);
		return "redirect:/admin/people";
	}

	@GetMapping(value = "/remove")
	public String removePerson(@RequestParam("email") String email) {
		User user = userService.findUserByEmail(email);
		if (user != null) {
			userService.removeUser(user);
		}
		return "redirect:/admin/people";
	}
}
