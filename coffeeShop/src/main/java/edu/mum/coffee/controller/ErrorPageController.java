package edu.mum.coffee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/error")
public class ErrorPageController {
	@RequestMapping("/404")
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String notFound() {
		return "/error/notFound";
	}

	@RequestMapping("/403")
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public String forbidden() {
		return "/error/forbidden";
	}
}
