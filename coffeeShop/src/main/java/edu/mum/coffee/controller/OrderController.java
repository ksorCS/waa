package edu.mum.coffee.controller;

import java.security.Principal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;

import edu.mum.coffee.domain.Order;
import edu.mum.coffee.domain.Person;
import edu.mum.coffee.domain.Product;
import edu.mum.coffee.service.OrderService;
import edu.mum.coffee.service.PersonService;
import edu.mum.coffee.service.ProductService;

@Controller
@RequestMapping("/orders")
@SessionAttributes(names= "customerOrder")
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private ProductService producService;
	@Autowired
	private PersonService personService;
	
	@GetMapping("/viewOrders")
	public String viewOrdersPage(Model model, HttpSession session) {
		Order order = (Order) session.getAttribute("customerOrder");
		if(order == null) {
			order = new Order();
			order.setOrderDate(new Date());
		}
		model.addAttribute("customerOrder", order);
		return "customer/viewOrders";
	}
	
	@GetMapping("/placeOrder")
	public String viewPlaceOrder(Model model, HttpSession session, Principal principal) {
		String userEmail = principal.getName();
		Person person = personService.findByEmail(userEmail).stream().findFirst().orElse(null);
		Order order = (Order) session.getAttribute("customerOrder");
		if(order == null) {
			order = new Order();
			order.setOrderDate(new Date());
		}
		order.setPerson(person);
		model.addAttribute("customerOrder", order);
		return "customer/placeOrder";
	}
	
	@PostMapping("/confirmOrder")
	public String confirmOrder(Model model, HttpSession session, Principal principal, WebRequest request, SessionStatus status) {
		Order order = (Order) session.getAttribute("customerOrder");
		orderService.save(order);
		status.setComplete();
		request.removeAttribute("customerOrder", WebRequest.SCOPE_SESSION);
		return "redirect:/";
	}
	
	@GetMapping("/addOrderLine")
	public String addOrderLine(@RequestParam(name ="productId") int productId, Model model, HttpSession session) {
		Product product = producService.getProduct(productId);
		Order order = (Order) session.getAttribute("customerOrder");
		if(order == null) {
			order = new Order();
			order.setOrderDate(new Date());
		}
		orderService.updateOrder(order, product);
		model.addAttribute("customerOrder", order);
		return "redirect:/";
	}
	
	@PostMapping(path ="/update", params= {"update"})
	public String updateOrder( HttpServletRequest request, Model model, HttpSession session) {
		int productId = Integer.valueOf(request.getParameter("productId"));
		int quantity = Integer.valueOf(request.getParameter("quantity"));
		Order order = (Order) session.getAttribute("customerOrder");
		if(order == null) {
			order = new Order();
			order.setOrderDate(new Date());
		}
		orderService.updateOrderLine(order, productId, quantity);
		model.addAttribute("customerOrder", order);
		return "redirect:/orders/viewOrders";
	}
	
	
	@PostMapping(path="/update", params= {"delete"})
	public String deleteOrderLine( HttpServletRequest request, Model model, HttpSession session) {
		int productId = Integer.valueOf(request.getParameter("productId"));
		Order order = (Order) session.getAttribute("customerOrder");
		if(order == null) {
			order = new Order();
			order.setOrderDate(new Date());
		}
		orderService.removeOrderLine(order, productId);
		model.addAttribute("customerOrder", order);
		return "redirect:/orders/viewOrders";
	}
	
}
