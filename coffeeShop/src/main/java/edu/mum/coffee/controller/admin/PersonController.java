package edu.mum.coffee.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.mum.coffee.domain.Person;
import edu.mum.coffee.service.PersonService;

@Controller(value ="adminPersonController")
@RequestMapping("/admin/people")
@SessionAttributes(names= "personId")
public class PersonController {
	@Autowired
	private PersonService personService;
	
	@GetMapping("")
	public String viewListPeoplePage(Model model) {
		model.addAttribute("persons", personService.getAllPeople());
		return "admin/person/list";
	}

	@GetMapping(value = "/new")
	public String redirectToNewPerson(Model model) {
		model.addAttribute("person", new Person());
		return "admin/person/new";
	}

	@GetMapping(value = "/edit/{id}")
	public String editPerson(@PathVariable("id") long id, Model model) {
		Person person = personService.findById(id);
		model.addAttribute("person", person);
		return "admin/person/new";
	}

	@PostMapping(value = "/save")
	public String savePerson(@ModelAttribute("person") Person person) {
		personService.savePerson(person);
		return "redirect:" + "/admin/people";
	}

	@GetMapping(value = "/changeStatus")
	public String disablePerson(@RequestParam("id") long id, @RequestParam("enable") boolean enable) {
		Person person = personService.findById(id);
		person.setEnable(enable);
		personService.savePerson(person);
		return "redirect:/admin/people";
	}
	
	@GetMapping(path="/api/all", produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Person> getAllPersons(){
		return personService.getAllPeople();
	}
	
	@GetMapping(path="/api/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Person getById(@PathVariable long id){
		return personService.findById(id);
	}
	
	@PostMapping(path="/api/add", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Person addPerson(@RequestBody Person person) {
		return personService.savePerson(person);
	}
	
	@PostMapping(path="/api/update", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Person updatePerson(@RequestBody Person person) {
		return personService.savePerson(person);
	}
	
	@DeleteMapping(path ="/api/delete/{id}")
	public void deletePerson(@PathVariable long id) {
		personService.removePerson(personService.findById(id));
	}
		
	
}
