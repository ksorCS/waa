package edu.mum.coffee.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.mum.coffee.domain.Product;
import edu.mum.coffee.service.ProductService;

@Controller(value ="adminProductController")
@RequestMapping("/admin/products")
public class ProductController {
	@Autowired
	private ProductService productService;

	@GetMapping("")
	public String viewListProductsPage(Model model) {
		model.addAttribute("products", productService.getAllProduct());
		return "admin/product/list";
	}

	@GetMapping(value = "/new")
	public String redirectToNewProduct(Model model) {
		model.addAttribute("product", new Product());
		return "admin/product/new";
	}

	@GetMapping(value = "/edit/{id}")
	public String editProduct(@PathVariable("id") int id, Model model) {
		Product product = productService.getProduct(id);
		model.addAttribute("product", product);
		return "admin/product/new";
	}

	@PostMapping(value = "/save")
	public String saveProduct(@ModelAttribute("product") Product product) {
		productService.save(product);
		return "redirect:" + "/admin/products";
	}

	@GetMapping(value = "/remove")
	public String removeProduct(@RequestParam("id") int id) {
		Product product = productService.getProduct(id);
		productService.delete(product);
		return "redirect:/admin/products";
	}

	@GetMapping(path = "/api/all", produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<Product> getAllProducts() {
		return productService.getAllProduct();
	}

	@GetMapping(path = "/api/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Product getById(@PathVariable int id) {
		return productService.getProduct(id);
	}

	@PostMapping(path = "/api/add", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Product addProduct(@RequestBody Product product) {
		return productService.save(product);
	}

	@PostMapping(path = "/api/update", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Product updateProduct(@RequestBody Product product) {
		return productService.save(product);
	}

	@DeleteMapping(path = "/api/delete/{id}")
	public void deleteProduct(@PathVariable int id) {
		productService.delete(productService.getProduct(id));
	}

}
