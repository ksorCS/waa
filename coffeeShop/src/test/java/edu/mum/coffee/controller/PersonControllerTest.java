package edu.mum.coffee.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import edu.mum.coffee.domain.Person;
import edu.mum.coffee.service.PersonService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PersonControllerTest {
	private static final String BASE_URL = "/admin/people/api";
	@MockBean
	private PersonService personService;
	@InjectMocks
	private PersonController personController;
	
	@Autowired
	private TestRestTemplate restTemplate;
	

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

	
	@Test
	public void shouldGetAllPersons() {
		List<Person> allPersons = getMockListPerson();
		when(personService.getAllPeople()).thenReturn(allPersons);
		String url = BASE_URL +"/all";
		Person[] persons = getRestTemplate().getForObject(url, Person[].class);
		assertThat(persons).containsAll(allPersons);
	}
	
	@Test
	public void shouldGetPersonById() {
		long id = 1l;
		Person person = getMockListPerson().get(0);
		when(personService.findById(id)).thenReturn(person);
		String url = BASE_URL +"/" + id;
		Person result = getRestTemplate().getForObject(url, Person.class);
		assertEquals(person, result);
	}
	
	@Test
	public void shouldAddNewPerson() {
		Person newPerson = getMockListPerson().get(0);
		when(personService.savePerson(Mockito.any(Person.class))).thenReturn(newPerson);
		String url = BASE_URL +"/add";
		ResponseEntity<Person> result = getRestTemplate().postForEntity(url, newPerson, Person.class);
		assertEquals(newPerson, result.getBody());
	}
	
	@Test
	public void shouldUpdatePerson() {
		Person person = getMockListPerson().get(1);
		when(personService.savePerson(Mockito.any(Person.class))).thenReturn(person);
		String url = BASE_URL +"/update";
		Person result = getRestTemplate().postForObject(url, person, Person.class);
		assertEquals(person, result);
	}
	
	@Test
	public void shouldDeletePerson() {
		when(personService.findById(Mockito.anyLong())).thenReturn(new Person());
		doNothing().when(personService).removePerson(Mockito.any(Person.class));
		String url = BASE_URL +"/delete/1";
		getRestTemplate().delete(URI.create(url));
		verify(personService).removePerson(Mockito.any(Person.class));
	}
	
	public List<Person> getMockListPerson(){
		Person p1 = new Person();
		p1.setEmail("abc@gmail.com");
		p1.setEnable(true);
		p1.setFirstName("Manh");
		p1.setFirstName("Phan");
		
		Person p2 = new Person();
		p1.setEmail("abx@gmail.com");
		p1.setEnable(true);
		p1.setFirstName("Mohasa");
		p1.setFirstName("Kim");
		return Arrays.asList(p1, p2);
	}
	
	public TestRestTemplate getRestTemplate() {
		return restTemplate.withBasicAuth("super@gmail.com	", "123456");
	}
	
}
