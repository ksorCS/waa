package edu.mum.coffee.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import edu.mum.coffee.domain.Product;
import edu.mum.coffee.domain.ProductType;
import edu.mum.coffee.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {
	private static final String BASE_URL = "/admin/products/api";
	@MockBean
	private ProductService productService;
	@InjectMocks
	private ProductController productController;
	
	@Autowired
	private TestRestTemplate restTemplate;
	

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

	
	@Test
	public void shouldGetAllProducts() {
		List<Product> allProducts = getMockListProduct();
		when(productService.getAllProduct()).thenReturn(allProducts);
		String url = BASE_URL + "/all";
		Product[] products = getRestTemplate().getForObject(url, Product[].class);
		assertThat(products).containsAll(allProducts);
	}
	
	@Test
	public void shouldGetProductById() {
		int id = 1;
		Product product = getMockListProduct().get(0);
		when(productService.getProduct(id)).thenReturn(product);
		String url = BASE_URL + "/" + id;
		Product result = getRestTemplate().getForObject(url, Product.class);
		assertEquals(product, result);
	}
	
	@Test
	public void shouldAddNewProduct() {
		Product newProduct = new Product();
		newProduct.setProductName("Bread");
		Product expectedProduct = new Product();
		expectedProduct.setProductName("Bread");
		when(productService.save(Mockito.any(Product.class))).thenReturn(expectedProduct);
		String url = BASE_URL + "/add";
		ResponseEntity<Product> result = getRestTemplate().postForEntity(url, newProduct, Product.class);
		assertEquals(expectedProduct, result.getBody());
	}
	
	@Test
	public void shouldUpdateProduct() {
		Product product = new Product();
		product.setProductName("ABC");
		when(productService.save(Mockito.any(Product.class))).thenReturn(product);
		String url = BASE_URL + "/update";
		Product result = getRestTemplate().postForObject(url, product, Product.class);
		assertEquals(product, result);
	}
	
	@Test
	public void shouldDeleteProduct() {
		when(productService.getProduct(Mockito.anyInt())).thenReturn(new Product());
		doNothing().when(productService).delete(Mockito.any(Product.class));
		String url = BASE_URL + "/delete/1";
		getRestTemplate().delete(URI.create(url));
		verify(productService).delete(Mockito.any(Product.class));
	}
	
	public List<Product> getMockListProduct(){
		Product p1 = new Product("Pho", "Pho VN", 12, ProductType.BREAKFAST);
		Product p2 = new Product("Dimsum", "Chinese dimsum", 12, ProductType.LUNCH);
		return Arrays.asList(p1, p2);
	}
	
	public TestRestTemplate getRestTemplate() {
		return restTemplate.withBasicAuth("super@gmail.com	", "123456");
	}
	
}
