package edu.mum.coffee.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import edu.mum.coffee.domain.Order;
import edu.mum.coffee.service.OrderService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class OrderControllerTest {
	private static final String BASE_URL = "/admin/orders/api";
	@MockBean
	private OrderService orderService;
	@InjectMocks
	private OrderController orderController;
	
	@Autowired
	private TestRestTemplate restTemplate;
	

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

	
	@Test
	public void shouldGetAllOrders() {
		List<Order> allOrders = getMockListOrder();
		when(orderService.findAll()).thenReturn(allOrders);
		String url = BASE_URL + "/all";
		Order[] orders = getRestTemplate().getForObject(url, Order[].class);
		assertThat(orders).containsAll(allOrders);
	}
	
	@Test
	public void shouldGetOrderById() {
		int id = 1;
		Order order = getMockListOrder().get(0);
		when(orderService.findById(id)).thenReturn(order);
		String url = BASE_URL + "/" + id;
		Order result = getRestTemplate().getForObject(url, Order.class);
		assertEquals(order, result);
	}
	
	@Test
	public void shouldAddNewOrder() {
		Order newOrder = new Order();
		when(orderService.save(Mockito.any(Order.class))).thenReturn(new Order());
		String url = BASE_URL + "/add";
		ResponseEntity<Order> result = getRestTemplate().postForEntity(url, newOrder, Order.class);
		assertEquals(newOrder, result.getBody());
	}
	
	@Test
	public void shouldUpdateOrder() {
		Order order = getMockListOrder().get(0);
		when(orderService.save(Mockito.any(Order.class))).thenReturn(order);
		String url = BASE_URL + "/update";
		Order result = getRestTemplate().postForObject(url, order, Order.class);
		assertEquals(order, result);
	}
	
	@Test
	public void shouldDeleteOrder() {
		when(orderService.findById(Mockito.anyInt())).thenReturn(new Order());
		doNothing().when(orderService).delete(Mockito.any(Order.class));
		String url = BASE_URL + "/delete/1";
		getRestTemplate().delete(URI.create(url));
		verify(orderService).delete(Mockito.any(Order.class));
	}
	
	public List<Order> getMockListOrder(){
		Order o1 = new Order();
		o1.setOrderDate(new Date());
		Order o2 = new Order();
		o2.setOrderDate(new Date());
		return Arrays.asList(o1, o2);
	}
	
	public TestRestTemplate getRestTemplate() {
		return restTemplate.withBasicAuth("super@gmail.com	", "123456");
	}
	
}
